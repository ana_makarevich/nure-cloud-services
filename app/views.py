#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
Definition of views.
"""

from django.shortcuts import render, redirect
from django.http import HttpRequest
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from usermanagement.views import UpdatePrefectInfo 
from datetime import datetime

from atracker.models import GroupReport
from django.core.urlresolvers import reverse

def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/index.html',
        context_instance = RequestContext(request,
        {
            'title':'Home Page',
            'year':datetime.now().year,
        })
    )

def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/contact.html',
        context_instance = RequestContext(request,
        {
            'title':'Contact',
            'message':'Your contact page.',
            'year':datetime.now().year,
        })
    )
@login_required
def myacc(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    context_dict = {
            'title':u'Мои сервисы',
            #'message':'Личный кабинет',
            'year':datetime.now().year,
        }
    template_name = 'app/myacc.html'
    if request.user.profile_type == "prefect":
        group = request.user.rp_profile.group
        if group is None:
            return redirect(reverse('usermanagement:prefect-edit'))
        else:
            reports = GroupReport.objects.filter(group = group)
            context_dict["reports"] = reports
            template_name = 'app/prefect_account.html'
    if request.user.profile_type == "fmember":
        template_name = 'app/dean_account.html'
    return render(
        request,
        template_name,
        context_instance = RequestContext(request,context_dict)
    )

def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    
    return render(
        request,
        'app/about.html',
        context_instance = RequestContext(request,
        {
            'title':'About',
            'message':'Your application description page.',
            'year':datetime.now().year,
        })
    )
