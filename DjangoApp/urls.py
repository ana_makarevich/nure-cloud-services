#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
Definition of urls for DjangoApp.
"""
from __future__ import unicode_literals
from datetime import datetime
from django.conf.urls import patterns, url,include
from app.forms import BootstrapAuthenticationForm
from app.views import *
from app.models import *
from usermanagement.models import *
from django.contrib.auth.views import *
from django.utils.translation import ugettext_lazy as _

# Uncomment the next lines to enable the admin:
from django.conf.urls import include
from django.contrib import admin
admin.autodiscover()


urlpatterns = [
    #url(r'^', include('app.urls', namespace="app")),
    url(r'^', include('usermanagement.urls', namespace="usermanagement")),
    url(r'^', include('atracker.urls', namespace="atracker")),
    url(r'^contact$', contact, name='contact'),
    url(r'^my$', myacc, name='myacc'),
    url(r'^about', about, name='about'),
    # Uncomment the admin/doc line below to enable admin documentation:
    #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    ]