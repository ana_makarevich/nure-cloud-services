#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.views.generic import View
from usermanagement.forms import BootstrapRegistrationForm,BootstrapAuthenticationForm, ProfileForm, ResponsiblePersonForm, CreateGroupForm, CreateUserForm, CreateRPForm
from django.shortcuts import render_to_response, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login
from django.views.generic.edit import UpdateView
from django.views.generic import CreateView
from usermanagement.models import CustomUser, ResponsiblePerson, Group


"""
View to login and register the user 
Supports post and get requests
POST request:
    If the user has not been activated, throws and error that is displayed on the same page
    If the provided credentials are invalid, throws and error that is displayed on the same page
GET request:
    If the get request was made after the registration then:
        - pre-populate the username field so that the user can login right away
        - show successfull registration notifcator
    If this is a new get request:
        - show login form
        - show registration form
Database effects:
    None

TODO: enable email activation
"""

class LoginUser(View):

    def post(self, request, *args, **kwargs):
        form = BootstrapAuthenticationForm(data = request.POST)
        registration_form = BootstrapRegistrationForm()
        context_dict = {'form': form, 'registration_form': registration_form, 'title':u"Войти",}
        context = RequestContext(request)
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request,user)
                return HttpResponseRedirect(reverse('myacc'))
            else:
                context_dict['user_inactive'] = u'Вы не активировали ваш аккаунт!'
                return render_to_response(
                    'usermanagement/login.html', 
                    context_dict, 
                    context)
        else:
            return render_to_response(
                'usermanagement/login.html', 
                context_dict, 
                context)

    def get(self, request, *args, **kwargs):

        registration_form = BootstrapRegistrationForm()
        context_dict = {'registration_form': registration_form, 'title':u"Войти",}
        # check if this get request was sent after successull registration
        context_dict['registered'] = request.session.pop('registered', None)
        # if the registration failed then extract errors to pass them further
        context_dict['reg_errors'] = request.session.pop('reg_errors',None)
        if context_dict['registered']:
            reg_data = request.session['reg_data']
            # if the user was registred succesfully, prepopulate the form
            form = BootstrapAuthenticationForm(initial={'username': reg_data['username']})
        else:
            form = BootstrapAuthenticationForm()
        context_dict['form'] = form
        context = RequestContext(request)
        return render_to_response('usermanagement/login.html', context_dict, context)

"""
View to register the user
Supports post method only
POST:
    - register user if he provided valid registration data (i.e. email and two password samples)
    - if the registration failed pass messages to the login view

Database effects:
    - adds new entry to CustomUser table

TODO: implement email activation, uncomment save to database

"""
class RegisterUser(View):            
    registered = False #will be set to True if the registration completes successfully
    template_name = 'usermanagement/login.html'
    def post(self, request, *args, **kwargs):
        context = RequestContext(request)    
        form = BootstrapAuthenticationForm()
        registration_form = BootstrapRegistrationForm(request.POST)
        if registration_form.is_valid():
            user = registration_form.save()
            user.email = user.username
            user.save()

            #user.set_password(user.password)  
            user.is_active = True
            #salt = hashlib.sha1(str(random.random())).hexdigest()[:5]            
            #activation_key = hashlib.sha1(salt+user.email).hexdigest()            
            #key_expires = datetime.datetime.today() + datetime.timedelta(2)
            #user.activation_key = activation_key
            #user.key_expires = key_expires	
            #email_subject = u'Подтвердите регистрацию в Nure Cloud Services'
            #message = "Нажмите на ссылку, чтобы активировать свою учетную запись http://atracker.azurewebsites.net/confirm/%s" % (activation_key)	
            #send_mail(email_subject, message, 'noreply@atracker.azurewebsites.net', [user.email], fail_silently=False)
            user.save()
            request.session['registered'] = True
            request.session['reg_data'] = request.POST
            return HttpResponseRedirect(reverse('usermanagement:login'))
        else:
            request.session['registered'] = False
            request.session['reg_errors'] = registration_form.errors
            return HttpResponseRedirect(reverse('usermanagement:login'))


class UpdateProfileView(UpdateView):
    model = CustomUser
    form_class = ProfileForm
    template_name = 'usermanagement/user_profile.html'
    def form_valid(self, form):
        user = form.save(commit=False)
        if user.profile_type == u"prefect" and not hasattr(user, 'rp_profile'):
            rp_profile = ResponsiblePerson(user = self.request.user)
            rp_profile.save()
        return super(UpdateProfileView, self).form_valid(form)
    def get_success_url(self):
        nextl = self.request.GET.get('next', None)
        if nextl:
            return reverse(nextl)
        else:
            return reverse('myacc')
    def get_object(self):
        return self.request.user
    def get_context_data(self, **kwargs):
        nextl = self.request.GET.get('next', None)
        context = super(UpdateProfileView, self).get_context_data(**kwargs)
        context['action'] = reverse('usermanagement:profile-edit')
        if nextl:
            context["next"] = nextl
        return context

# TODO: check why the department is not saved
class UpdatePrefectInfo(UpdateView):
    model = ResponsiblePerson
    form_class = ResponsiblePersonForm 
    template_name = 'usermanagement/prefect_profile.html'
    def get_object(self):
        return self.request.user.rp_profile
    def get_context_data(self, **kwargs):
        context = super(UpdatePrefectInfo, self).get_context_data(**kwargs)
        context['action'] = reverse('usermanagement:prefect-edit')
        return context
    def get_success_url(self):
        return reverse('myacc')

class CreateGroupView(CreateView):
    model = Group 
    form_class = CreateGroupForm
    template_name = 'usermanagement/edit_group.html'
    def context_date(self, **kwargs):
        context = super(UpdatePrefectInfo, self).get_context_data(**kwargs)
        context['action'] = reverse('usermanagement:group-create')
        return context
    def get_success_url(self):
        if self.request.user.profile_type == u"prefect" and not hasattr(self.request.user, 'rp_profile'): 
            return reverse('usermanagement:prefect-edit')
        else:
            return reverse('usermanagement:prefect-create')
    
class CreatePrefectView(View):
    def get(self, request, *args, **kwargs):
        userForm = CreateUserForm
        rpForm = CreateRPForm
        context_dict = {}
        context_dict["userForm"] = userForm
        context_dict["rpForm"] = rpForm
        context = RequestContext(request)
        return render_to_response('usermanagement/add_prefect.html', context_dict, context)
    def post(self, request, *args, **kwargs):
        userForm = CreateUserForm(data = request.POST)
        rpForm = CreateRPForm(data = request.POST)
        if userForm.is_valid() and rpForm.is_valid():
            print(rpForm.cleaned_data['group'])
            user = userForm.save()
            user.username = user.email
            user.profile_type = u'prefect'
            user.set_password('0000')
            user.save()
            rp = ResponsiblePerson(
                user = user,
                group = rpForm.cleaned_data['group'],
                phone_number = rpForm.cleaned_data['phone_number']
                )
            rp.save()
            return HttpResponseRedirect(reverse('myacc'))
        else:
            context_dict = {}
            context_dict["userForm"] = userForm
            context_dict["rpForm"] = rpForm
            context = RequestContext(request)
            return render_to_response('usermanagement/add_prefect.html', context_dict, context)
            
    
        
    