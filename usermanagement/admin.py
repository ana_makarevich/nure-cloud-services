#!/usr/bin/env python
# -*- coding:utf-8 -*-

from __future__ import unicode_literals

from django.contrib import admin
from usermanagement.models import CustomUser, Group, ResponsiblePerson
from django.utils.translation import ugettext_lazy as _

class UserAdmin(admin.ModelAdmin):					

	list_display = (
		'username',
		'f_name',
		'email',
		'is_active',
		'profile_type',
		)

	list_filter = (
		'profile_type',
		)	

	readonly_fields = (
		'date_joined',
		'last_login',
		'key_expires',
		)			

	def f_name(self, obj):
		return ("%s %s" % (obj.first_name, obj.last_name))		

	f_name.short_description = u'Полное имя'	
class GroupsAdmin(admin.ModelAdmin):
	list_display = (
		'group_name',
		'faculty',
		'department',
		)
	
	list_filter = (
		'faculty',
		)
class ResponsiblePersonAdmin(admin.ModelAdmin):
	list_display = (
		'user',
		'group',
		)
	list_filter = (
		'group',
		)
admin.site.register(CustomUser, UserAdmin)
admin.site.register(Group, GroupsAdmin)
admin.site.register(ResponsiblePerson, ResponsiblePersonAdmin)