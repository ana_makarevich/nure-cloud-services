#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
Definition of urls for DjangoApp.
"""
from __future__ import unicode_literals
from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse_lazy
from usermanagement.views import RegisterUser, LoginUser, UpdateProfileView,\
	UpdatePrefectInfo, CreateGroupView, CreatePrefectView
from django.contrib.auth.views import logout

# Uncomment the next lines to enable the admin:
from django.contrib import admin
from django.contrib.auth.decorators import user_passes_test

# Where to redirect user if he is already logged in
login_forbidden =  user_passes_test(lambda u: u.is_anonymous(), reverse_lazy('myacc'), redirect_field_name=None)

admin.autodiscover()

urlpatterns = patterns('',
	     url(r'^$', login_forbidden(LoginUser.as_view()), name='login'),
     url(r'^logout$', logout, {  'next_page': '/'  },        name='logout'),
     url(r'^register$', RegisterUser.as_view(), name = 'register'),
     url(r'^edit_profile/$', UpdateProfileView.as_view(), name = 'profile-edit'),
     url(r'^edit_prefect_profile/$', UpdatePrefectInfo.as_view(), name = 'prefect-edit'),
     url(r'^create_group/$', CreateGroupView.as_view(), name = 'group-create'),
     url(r'^add_prefect/$', CreatePrefectView.as_view(), name = 'prefect-create')

    )