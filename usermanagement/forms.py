#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.utils.translation import ugettext_lazy as _
from usermanagement.models import CustomUser, ResponsiblePerson, Group

attributes = {'class': 'form-control', 'required': 'true','onchange':"this.setCustomValidity('')", 'oninput':"this.setCustomValidity('')"}

class BootstrapAuthenticationForm(AuthenticationForm):
    """Authentication form which uses boostrap CSS."""
    attrs = attributes.copy()
    attrs.update({
        'placeholder': 'Логин (адрес эл.почты)',
        'oninvalid':"this.setCustomValidity('Это поле не может быть пустым')",
        })
    username = forms.CharField(max_length=254,
                               widget=forms.TextInput(attrs))
    #attrs = attributes.copy()
    attrs.update({
        'placeholder': 'Пароль',
        })
    password = forms.CharField(label=_("Password"),
                               widget=forms.PasswordInput(attrs))

class BootstrapRegistrationForm(UserCreationForm):    
    attrs = attributes.copy()    
    attrs.update({
        'placeholder': 'Адрес эл.почты',
        'oninvalid':"this.setCustomValidity('Укажите корректный адрес эл. почты')",
        })    
    username = forms.EmailField(max_length=254,
        widget=forms.EmailInput(attrs))
    attrs.update({
        'placeholder': 'Пароль',
        'oninvalid':"this.setCustomValidity('Это поле не может быть пустым')",
        })        
    password1 = forms.CharField(label=_("Password1"),
        widget=forms.PasswordInput(attrs))
    attrs.update({
        'placeholder': 'Пароль (еще раз)',
        'oninvalid':"this.setCustomValidity('Это поле не может быть пустым')",
        })
    password2 = forms.CharField(label=_("Password2"),
        widget=forms.PasswordInput(attrs))    
    class Meta:
        model = CustomUser
        fields = ("username", )
        
class ProfileForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        
        fields = ('first_name', 'middle_name', 'last_name', 'email', 'username','profile_type', 'faculty', 'department')
        
        widgets = {
        'first_name': forms.TextInput(attrs={'class': 'form-control', 'required': ''}),
        'middle_name': forms.TextInput(attrs={'class': 'form-control'}),
        'last_name': forms.TextInput(attrs={'class': 'form-control', 'required': ''}),
        'username': forms.TextInput(attrs={'class': 'form-control', 'required': ''}),
        'email': forms.EmailInput(attrs={'class': 'form-control', 'required': ''}),
        'profile_type': forms.Select(attrs = {'class': 'form-control', 'required': ''}),
        'faculty': forms.Select(attrs = {'class': 'form-control', 'required': ''}),
        'department': forms.Select(attrs = {'class': 'form-control', 'required': ''})
        }
        
        labels = {
        'first_name':_(u'Имя'),
        'middle_name':_(u'Отчество'),
        'last_name':_(u'Фамилия'),
        'email': _(u"E-mail"),
        'username': _(u'Логин'),
        'profile_type': _(u'Тип профиля'),
        'faculty': _(u'Факультет'),
        'department': _(u'Кафедра (не обязательно)'),
        }
        help_texts = {
            'username': ""
            }
        
class ResponsiblePersonForm(forms.ModelForm):
    class Meta:
        model = ResponsiblePerson 
        fields = ('group', 'phone_number')
        widgets = {
            'group': forms.Select(attrs={'class': 'form-control', 'placeholder': u'xxx', 'required': ''}),
            'phone_number': forms.TextInput(attrs={'class': 'form-control', 'placeholder': u'Телефон (не обязательно)'})}
        labels = {
            'group': _(u'Группа'),
            'phone_number': _(u'Номер телефона'),
            }
    
class CreateGroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ('group_name', 'current_study_year', 'faculty', 'department')
        widgets = {
            'group_name': forms.TextInput(attrs={'class': 'form-control', 'required': ''}),
            'current_study_year': forms.NumberInput(attrs={'class': 'form-control', 'required': ''}),
            'faculty': forms.Select(attrs={'class': 'form-control', 'required': ''}),
            'department': forms.Select(attrs={'class': 'form-control', 'required': ''})
            }
        labels = {
            'group_name': _(u'Группа'),
            'current_study_year': _(u'Текущий год обучения'),
            'faculty': _(u'Факультет'),
            'department': _(u'Кафедра')
            }        
class CreateUserForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ('first_name', 'middle_name', 'last_name', 'email', 'faculty', 'department')
        widgets = {
            'first_name': forms.TextInput(attrs = {'class': 'form-control', 'required': ''} ),
            'middle_name': forms.TextInput(attrs = {'class': 'form-control'}),
            'last_name': forms.TextInput(attrs = {'class': 'form-control', 'required': ''}),
            'email': forms.EmailInput(attrs = {'class': 'form-control', 'required': ''}),
            'faculty': forms.Select(attrs = {'class': 'form-control', 'required': ''}),
            'department': forms.Select(attrs = {'class': 'form-control', 'required': ''})
            }
        labels = {
            'first_name': _(u'Имя*'),
            'middle_name': _(u'Отчество'),
            'last_name': _(u'Фамилия*'),
            'email': _(u'E-mail*'),
            'faculty': _(u'Факультет*'),
            'department': _(u'Кафедра*')            
            }
    def clean_email(self):
        email = self.cleaned_data.get('email')
        username = self.cleaned_data.get('username')
        if email and CustomUser.objects.filter(email=email).exclude(username=username).count():
            raise forms.ValidationError(u'Такой адрес почты уже существует.')
        return email
        
class CreateRPForm(forms.ModelForm):
    class Meta:
        model = ResponsiblePerson
        fields = ('group','phone_number')
        widgets = {
            'group': forms.Select(attrs = {'class': 'form-control', 'required': ''}),
            'phone_number': forms.TextInput(attrs = {'class': 'form-control'})
            }
        labels = {
            'group': _(u'Группа*'),
            'phone_number': _(u'Телефон')
            }
    