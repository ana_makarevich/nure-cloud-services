#!/usr/bin/env python
# -*- coding:utf-8 -*-

from __future__ import unicode_literals
# Django libraries
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.encoding import smart_unicode
from django.utils import timezone
from django.core.validators import MinValueValidator, MaxValueValidator
from unittest.util import _MAX_LENGTH


# Custom User class that enhances the capabilities of the AbstracUser

CS = u'cs'
CE = u'cem'
NONE = u'none'
AI = u"ai"
ST = u"st"
ICS = u"ics"
MST = u"mst"
SE = u"se"
FACULTIES = (
	(CS, u"КН"),
	(CE, u"КИУ"),
	(NONE, u"Неизвестно")
	)
DEPARTMENTS = (
	(AI, u"ИИ"),
	(ST, u"СТ"),
	(ICS, u"ИУС"),
	(MST, u"МСТ"),
	(SE, u"ПИ"),
	(NONE, u"Неизвестно")
	)	
class CustomUser(AbstractUser):

	PREFECT = u'prefect'
	FACULTY_MEMBER = u'fmember'
	ADMIN = u'admin'
	UNDEF = u'undef'

	ACCOUNT_TYPE_CHOICES = (
		(PREFECT, u"Староста"),
		(FACULTY_MEMBER, u"Сотрудник деканата"),
		(ADMIN, u"Администратор"),
		(UNDEF, u"Неопределившийся")
		)
	middle_name = models.CharField(
		max_length = 100,
		blank = True,
		null = True,
		default = None
		)
	# Данные для подтверждения по почте
	activation_key = models.CharField(
		max_length=100,
		blank=True, 
		verbose_name = u'Ключ активации')

	key_expires = models.DateTimeField(
		default=timezone.now, 
		verbose_name = u'Срок действия ключа')

	# Account type: prefect, faculty member
	profile_type = models.CharField(
		max_length=50, 
		choices = ACCOUNT_TYPE_CHOICES, 
		default = UNDEF)
	
	approved = models.BooleanField(
		default = False,
		verbose_name = u"Личность подтверждена"
		)
	faculty = models.CharField(
		max_length = 200,
		choices = FACULTIES,
		default = CS,
		blank = True
		)
	
	department = models.CharField(
		max_length = 200,
		choices = DEPARTMENTS,
		blank = True,
		default = None
		)
	
	def __unicode__(self):
		return smart_unicode(self.get_full_name())

class Group(models.Model):
	
	group_name = models.CharField(
		max_length = 50, unique = True
		)
	
	faculty = models.CharField(
		max_length = 200,
		choices = FACULTIES,
		default = CS,
		blank = True
		)
	
	department = models.CharField(
		max_length = 200,
		choices = DEPARTMENTS,
		blank = True,
		default = None
		) 
	current_study_year = models.PositiveSmallIntegerField(
		validators=[MinValueValidator(1), MaxValueValidator(5)],
		verbose_name = "Курс", default = 3)
	def __unicode__(self):
		return self.group_name

# TODO: add the link to the last report and to group reports maybe 
class ResponsiblePerson(models.Model):
	user = models.OneToOneField(CustomUser, related_name = 'rp_profile')
	phone_number = models.CharField(max_length = 20, default = None, blank = True, null = True)
	status_confirmed = models.BooleanField(default = False)
	# group can have multiple responsible persons
	group = models.ForeignKey(Group, blank = True, null = True, default = None)
