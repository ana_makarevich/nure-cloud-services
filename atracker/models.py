#!/usr/bin/env python
# -*- coding:utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from usermanagement.models import Group, CustomUser
from django.utils import timezone

# report stamp: who, where, when 
class GroupReport(models.Model):
    group = models.ForeignKey(
        Group,
        verbose_name = u"Группа"
        )
    # who filled in the form
    responsible_person = models.ForeignKey(
        CustomUser,
        verbose_name = u"Кто заполнил"
        ) 
    term = models.PositiveSmallIntegerField(
        blank = True, 
        default = None,
        verbose_name = u"Семестр"
        )
    month = models.PositiveSmallIntegerField(
        verbose_name = u"Месяц")
    year_of_study = models.PositiveSmallIntegerField(
        default = 1,
        verbose_name = u"Год обучения"
        )
    year = models.PositiveSmallIntegerField(
        default = 2016, 
        blank = True, 
        null = True,
        verbose_name = u"Год")
    timestamp = models.DateTimeField(
        default=timezone.now, 
        verbose_name = u'Заполнено')
    edited = models.DateTimeField(
        default=timezone.now, 
        verbose_name = u'Отредактировано') 
    problems = models.BooleanField(
        default = False,
        verbose_name = u"Есть проблемы"
        )
    seen = models.BooleanField(
        default = False,
        verbose_name = u"Просмотрено"
        )
    def __unicode(self):
        return (self.month + u"-" + self.year)

class Student(models.Model):
    journal_id = models.PositiveSmallIntegerField(
        default = None,
        blank = True,
        null  = True
        )
    first_name = models.CharField(
        max_length = 100,
        verbose_name = u"Имя")
    middle_name =  models.CharField(
        max_length = 100, 
        blank=True, 
        default = None,
        verbose_name = u"Отчество"
        )
    last_name = models.CharField(
        max_length = 100,
        verbose_name = u"Фамилия"
        )
    group = models.ForeignKey(
        Group,
        verbose_name = u"Группа")
    active = models.BooleanField(
        default = True,
        verbose_name = u"Активен")
    # this student requires attention (judging from his overall absences for the current term)
    # calculated automatically
    phone_number = models.CharField(
        max_length = 20,
        default = None,
        blank= True,
        null = True,
        verbose_name = u"Номер телефона"
        )
    """
    day_of_birth = models.PositiveSmallIntegerField(
        default  = None,
        blank = True,
        null = True,
        verbose_name = u"День рождения")
    month_of_birth = models.PositiveSmallIntegerField(
        default = None,
        blank = True,
        null = True,
        verbose_name = u"Месяц рождения")
    year_of_birth = models.PositiveSmallIntegerField(
        default = None,
        blank = True,
        null = True,
        verbose_name = u"Год рождения"
        )
    """
    date_of_birth = models.DateField(
        default = None,
        blank = True,
        null = True,
        verbose_name = u"Дата рождения"
        )
    address = models.CharField(
        max_length = 300,
        default = None,
        blank = True,
        null = True,
        verbose_name = u"Адрес"
        )
    
    requires_attention = models.BooleanField(
        default = False,
        verbose_name = u"Злостный прогульщик")
    
    
    def __unicode__(self):
        return (self.last_name + u" " + self.first_name[0] + ".")
    

class StudentReport(models.Model):
    report = models.ForeignKey(
        GroupReport,
        verbose_name = u"Отчет (месяц-год)"
        )
    student = models.ForeignKey(
        Student,
        verbose_name = u"Студент")
    absence_days = models.PositiveSmallIntegerField(
        default = 0,
        verbose_name = u"Всего пропущено"
        )
    ill = models.PositiveSmallIntegerField(
        default = 0,
        verbose_name = u"По болезни")
    other_reason = models.PositiveSmallIntegerField(
        default = 0,
        verbose_name = u"По другим причинам"
        )
    no_reason = models.PositiveSmallIntegerField(
        default = 0, 
        verbose_name = u"Без причины")
    comments = models.TextField(
        verbose_name = u"Специальные отметки", default = None, blank = True, null = True)
    requires_attention = models.BooleanField(
        default = False,
        verbose_name = u"Злостный прогульщик")
    