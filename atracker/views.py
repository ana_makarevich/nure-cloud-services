#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
Definition of views.
"""

# Стандартные библиотеки
from __future__ import unicode_literals
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic import View
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse
from atracker.models import Student, GroupReport, StudentReport
from atracker.forms import CreateStudentForm, CreateGroupReportForm, StudentReportFormSet
from django.db.models import Q
from datetime import date
from django.http import HttpResponseRedirect
# Create your views here.
from django.template.defaulttags import register
from usermanagement.models import ResponsiblePerson, Group

@register.filter(name='lookup')
def cut(value, arg):
    return value[arg]

class StudentsList(View):
    template_name = 'stud_list.html'
    @method_decorator(login_required)  
    def get(self, request, *args, **kwargs):
        user = request.user
        context_dict = {}
        if user.rp_profile:
            rp_profile = user.rp_profile
            context_dict["status"] = "OK"
            context_dict["group"] = rp_profile.group
            students = Student.objects.filter(group = rp_profile.group)
            context_dict["students"] = students
        else:
            context_dict["status"] = "Reject"
        context = RequestContext(request)                 
        return render_to_response(
            self.template_name,
            context_dict, context)
        
class CreateStudentView(CreateView):
    model = Student
    form_class = CreateStudentForm
    template_name = 'edit_student.html'
    
    def form_valid(self, form):
        student = form.save(commit=False)
        student.group = self.request.user.rp_profile.group
        #if not student.year_of_birth is None and not student.month_of_birth is None and not student.day_of_birth is None:
        #    student.date_of_birth = date(student.year_of_birth, student.month_of_birth, student.day_of_birth)
        print(student.date_of_birth)
        return super(CreateStudentView, self).form_valid(form)
    def get_success_url(self):
        return reverse('atracker:stud_list')

    def get_context_data(self, **kwargs):

        context = super(CreateStudentView, self).get_context_data(**kwargs)
        context['action'] = reverse('atracker:student-new')

        return context

class UpdateStudentView(UpdateView):
    model = Student
    form_class = CreateStudentForm
    template_name = 'edit_student.html'
    def form_valid(self, form):
        student = form.save(commit=False)
        #if not student.year_of_birth is None and not student.month_of_birth is None and not student.day_of_birth is None:
        #    student.date_of_birth = date(student.year_of_birth, student.month_of_birth, student.day_of_birth)
        #else:
        #    student.date_of_birth = None
        return super(UpdateStudentView, self).form_valid(form)
    def get_success_url(self):
        return reverse('atracker:stud_list')

    def get_context_data(self, **kwargs):

        context = super(UpdateStudentView, self).get_context_data(**kwargs)
        context['action'] = reverse('atracker:student-edit', kwargs={'pk': self.get_object().id})

        return context
    
class CreateGroupReportView(CreateView):
    model = GroupReport
    form_class = CreateGroupReportForm
    template_name = 'add_report.html'
    repid = None
    def form_valid(self, form):
        group = self.request.user.rp_profile.group
        report = form.save(commit=False)
        report.group = group
        report.responsible_person = self.request.user
        response = super(CreateGroupReportView, self).form_valid(form)
        students = Student.objects.filter(group = group)
        for student in students:
            student_report = StudentReport(report = report, student = student, comments="")
            student_report.save()
        return response
    
    def get_success_url(self):
        return reverse("atracker:student-reports-edit", kwargs={'pk': self.object.id})
    
    def get_context_data(self, **kwargs):
        context = super(CreateGroupReportView, self).get_context_data(**kwargs)
        context['action'] = reverse('atracker:report-new')
        return context

class CreateStudentReports(View):
    template_name = 'fill_in_reports.html'
    def get(self, request, *args, **kwargs):
        group = request.user.rp_profile.group
        students = Student.objects.filter(group = group)
        report_id = kwargs.get("pk")
        reports = StudentReport.objects.filter(report = report_id)
        formset =  StudentReportFormSet(queryset = reports)
        context_dict = {}
        context_dict["formset"] = formset
        context_dict["students"] = { x.id: x for x in students }
        context_dict["report"] = GroupReport.objects.get(id = report_id)
        return render(request, self.template_name,context_dict)
    
    def post(self, request, *args, **kwargs):
        #group = request.user.rp_profile.group
        formset =  StudentReportFormSet(request.POST)
        formset.save()
        return HttpResponseRedirect(reverse('myacc'))

class DeleteGroupReport(DeleteView):
    model = GroupReport
    template_name = 'atracker/delete_report.html'
    
    def get_success_ulr(self):
        return reverse('myacc') 
            
class PrefectsList(View):
    template_name = 'pref_list.html'
    def get(self, request, *args, **kwargs):
        context_dict = {}
        if request.user.profile_type == "fmember" and (request.user.faculty == "" or request.user.faculty == u"none"):
            context_dict["next"] = 'atracker:prefects-list'
            context_dict["no_faculty"] = True
        else:
            fac = request.user.faculty
            context_dict["no_faculty"] = False
            rpeople = ResponsiblePerson.objects.filter(user__faculty = fac)
            context_dict["rpeople"] = rpeople
            """
            no_fac_people = ResponsiblePerson.objects.filter(Q(user__faculty = u"") | 
                                                             Q(user__faculty = u"none") | 
                                                             Q(user__faculty = None))
            #no_fac_people = ResponsiblePerson.objects.all()
            
            context_dict["no_fac_people"] = no_fac_people
            """ 
        return render(request, self.template_name,context_dict)

class GroupsList(View):
    template_name = 'groups_list.html'
    def get(self, request, *args, **kwargs):
        context_dict = {}
        if request.user.profile_type == "fmember" and (request.user.faculty == "" or request.user.faculty == u"none"):
            context_dict["next"] = 'atracker:prefects-list'
            context_dict["no_faculty"] = True
        else:
            fac = request.user.faculty
            context_dict["no_faculty"] = False
            students = Student.objects.filter(group__faculty = fac)
            context_dict["students"] = students
        return render(request, self.template_name,context_dict)

class GroupInfo(View):
    template_name = 'group_info.html'
    def get(self, request, *args, **kwargs):
        context_dict = {}  
        group_id = kwargs.get("pk")
        group = Group.objects.get(id = group_id)
        students = Student.objects.filter(group = group)
        context_dict['students'] = students
        context_dict["group"] = group
        return render(request, self.template_name,context_dict) 