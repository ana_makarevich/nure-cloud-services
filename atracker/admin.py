from __future__ import unicode_literals


from django.contrib import admin
from atracker.models import GroupReport, Student, StudentReport
from django.utils.translation import ugettext_lazy as _

class StudentAdmin(admin.ModelAdmin):
    list_display = (
        'first_name',
        'last_name',
        'group',
        'requires_attention',
        )
    list_filter = (
        'requires_attention',
        )

class StudentReportAdmin(admin.ModelAdmin):
    list_display = (
        'report',
        'absence_days',
        'ill',
        'other_reason',
        'no_reason',
        'requires_attention',
        )
class ReportAdmin(admin.ModelAdmin):
    list_display = (
        'year',
        'month',
        )
admin.site.register(Student, StudentAdmin)
admin.site.register(StudentReport, StudentReportAdmin)
admin.site.register(GroupReport, ReportAdmin)