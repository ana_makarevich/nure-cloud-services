#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
Created on 6 Dec 2016

@author: ana_makarevich
'''
# Стандартные библиотеки
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django import forms
from atracker.models import Student, GroupReport, StudentReport
from django.forms import formset_factory, modelformset_factory
from datetime import date
attributes = {'class': 'form-control', 'required': 'true','onchange':"this.setCustomValidity('')", 'oninput':"this.setCustomValidity('')"}

StudentReportFormSet = modelformset_factory(StudentReport, 
                                                    fields = (
                                                            'student',
                                                              'absence_days', 
                                                              'ill',
                                                              'other_reason',
                                                              'comments'), 
                                                    widgets = {
                                                        'student': forms.HiddenInput(),
                                                        'absence_days': forms.NumberInput(attrs = {'class': 'form-control'}),
                                                        'ill': forms.NumberInput(attrs = {'class': 'form-control'}),
                                                        'other_reason': forms.NumberInput(attrs = {'class': 'form-control'}),
                                                        'comments': forms.TextInput(attrs = {'class': 'form-control'})},
                                                    extra = 0)
class CreateStudentForm(forms.ModelForm):
    class Meta:
        model = Student
        #date(1900, month, 1).strftime('%B')
        years = [(year, year) for year in range(1990,2006)]
        months_names = [u'Январь',u'Февраль',u'Март',u'Апрель',u'Май',u'Июнь',u'Июль',u'Август',u'Сентябрь',u'Октябрь',u'Ноябрь',u'Декабрь']
        #months = [(month, date(1900, month, 1).strftime('%B')) for month in range(1,12)]
        months = [(month, months_names[month-1]) for month in range(1,13)]
        days = [(day, day) for day in range(1,32)]
        fields = ('journal_id', 'first_name', 'middle_name', 'last_name','date_of_birth', 
                  'phone_number', 'address')
        widgets = {
        'journal_id': forms.TextInput(attrs={'class': 'form-control', 'placeholder': u'Номер в журнале'}),
        'first_name': forms.TextInput(attrs={'class': 'form-control','required': 'true', 'placeholder': u'Имя','required': 'true','onchange':"this.setCustomValidity('')", 'oninput':"this.setCustomValidity('')"}),
        'middle_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': u'Отчество'}),
        'last_name': forms.TextInput(attrs={'class': 'form-control', 'required': 'true', 'placeholder': u'Фамилия', 'required': 'true','onchange':"this.setCustomValidity('')", 'oninput':"this.setCustomValidity('')"}),
        'phone_number': forms.TextInput(attrs={'class': 'form-control', 'placeholder': u'Телефон'}),
        #'day_of_birth': forms.Select(attrs={'class': 'form-control', 'placeholder': u'День'}, choices = days),
        #'month_of_birth': forms.Select(attrs={'class': 'form-control', 'placeholder': u'Месяц'}, choices = months),
        #'year_of_birth': forms.Select(attrs={'class': 'form-control', 'placeholder': u'Год'}, choices = years),
        'date_of_birth': forms.DateInput(attrs={'class': 'form-control', 'min':"1990-01-01", 'max':"2005-01-01", 'type':"date"}),
        'address': forms.TextInput(attrs={'class': 'form-control', 'placeholder': u'Адрес'}),
        }
        
        labels = {
        'journal_id':_(u'Номер в журнале'),
        'first_name':_(u'Имя'),
        'middle_name':_(u'Отчество'),
        'last_name':_(u'Фамилия'),
        'phone_number':_(u'Телефон'),
        #'day_of_birth':_(u''),
        #'month_of_birth':_(u''),
        #'year_of_birth':_(u""),
        'address':_(u'Адрес'),
        }

class UpdateStudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ('journal_id', 'first_name', 'middle_name', 'last_name', 'active')
        widgets = {
        'journal_id': forms.TextInput(attrs={'class': 'form-control'}),
        'first_name': forms.TextInput(attrs={'class': 'form-control'}),
        'middle_name': forms.TextInput(attrs={'class': 'form-control'}),
        'last_name': forms.TextInput(attrs={'class': 'form-control'}),
        'active': forms.CheckboxInput(),
        }
        labels = {
        'journal_id':_(u'Номер в журнале'),
        'first_name':_(u'Имя'),
        'middle_name':_(u'Отчество'),
        'last_name':_(u'Фамилия'),
        'active': _(u"Активен (НЕ отчислен)")
        }
        
        
class CreateGroupReportForm(forms.ModelForm):
    class Meta:
        model = GroupReport
        fields = ('term', 'year_of_study', 'month', 'year')
        widgets = {
            'term': forms.TextInput(attrs={'class': 'form-control'}),
            'year_of_study': forms.TextInput(attrs={'class': 'form-control'}),
            'month': forms.TextInput(attrs={'class': 'form-control'}),
            'year': forms.TextInput(attrs={'class': 'form-control'}),
            }
        labels = {
            'term':_(u'Текущий семестр'),
            'year_of_study':_(u'Курс'),
            'month':_(u'Месяц'),
            'year':_('Год')
            }

class StudentReportForm(forms.ModelForm):
    class Meta:
        model = StudentReport
        fields = ('report',
                  'student', 
                  'absence_days',
                  'ill',
                  'other_reason',
                  'no_reason',
                  'comments')
        widgets = {
            'report': forms.HiddenInput(),
            'student': forms.HiddenInput(),
            'absence_days': forms.NumberInput(attrs = {'class': 'form-control'}),
            'ill': forms.NumberInput(attrs = {'class': 'form-control'}),
            'other_reason': forms.NumberInput(attrs = {'class': 'form-control'}),
            'no_reson': forms.NumberInput(attrs = {'class': 'form-control'}),
            'comments': forms.NumberInput(attrs = {'class': 'form-control'}),
            }
