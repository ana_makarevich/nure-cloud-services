#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
Definition of urls for DjangoApp.
"""
from __future__ import unicode_literals
from django.conf.urls import patterns, url
from atracker.views import StudentsList, CreateStudentView, UpdateStudentView , CreateGroupReportView,\
    CreateStudentReports, DeleteGroupReport, PrefectsList, GroupsList, GroupInfo 

urlpatterns = patterns('',
     url(r'^students_list$', StudentsList.as_view(), name = 'stud_list'),
     url(r'^create_student$', CreateStudentView.as_view(), name = 'student-new'),
     url(r'^edit_student/(?P<pk>\d+)/$', UpdateStudentView.as_view(), name='student-edit',),
     url(r'^create_report$', CreateGroupReportView.as_view(), name = 'report-new'),
     url(r'^delete_report/(?P<pk>\d+)/$', DeleteGroupReport.as_view(), name='report-delete',),
     url(r'^create_student_reports/(?P<pk>\d+)/$', CreateStudentReports.as_view(), name = 'student-reports-edit'),
     url(r'^prefects_list$', PrefectsList.as_view(), name = 'prefects-list'),
     url(r'^groups_list$', GroupsList.as_view(), name = 'groups-list'),
     url(r'^group_info/(?P<pk>\d+)/$', GroupInfo.as_view(), name = 'group-info'),
    )
